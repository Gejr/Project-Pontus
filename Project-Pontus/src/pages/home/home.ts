import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import {AddPlacePage} from "../add-place/add-place";
import {SetLocationPage} from "../set-location/set-location";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  addPlacePage = AddPlacePage;
  setLocationPage = SetLocationPage;
  constructor(public navCtrl: NavController) {

  }

}
